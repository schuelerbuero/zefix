<?php
/**
 * This file is responsible for rendering the homepage that ist set in settings->reading
 */
get_header(); ?>
<div class="content">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<h2 class="content-headline"><?php the_title(); ?></h2>
 <div class="content-content">
    <?php the_content(); ?>
 </div>
<?php endwhile; endif; ?>

</div><!-- /content -->

<?php
get_sidebar();
get_footer();
?>
