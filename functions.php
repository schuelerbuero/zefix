<?php
function zefix_register_my_menu() {
  register_nav_menu('main-menu',__( 'Hauptmenü' ));
}
add_action( 'init', 'zefix_register_my_menu' );


function zefix_register_sidebar(){
  register_sidebar( array(
        'name' => __( 'Sidebar', 'zefix' ),
        'id' => 'sidebar',
        'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'zefix' ),
        'before_widget' => '<div class="sidebar-item">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="sidebar-headline">',
        'after_title' => '</h3>',
    ) );


}

add_action( 'widgets_init', 'zefix_register_sidebar' );


add_theme_support( 'post-thumbnails' );
add_image_size( 'post-thumbnail', 300, 120, true );
