<?php get_header(); ?>
<div class="content">
<h2 class="content-headline">Obacht! Leider nix gefunden.</h2>
 <div class="content-content">
    <p>
      Der von dir ausgewählte Inhalt konnte leider nicht gefunden werden. Entweder ist die Seite nicht mehr verfügbar oder es gab sie noch nie. Stattdessen haben wir uns entschieden, dir einfach Blindtext anzuzeigen:
    </p>
    <p>
      Eine wunderbare Heiterkeit hat meine ganze Seele eingenommen, gleich den süßen Frühlingsmorgen, die ich mit ganzem Herzen genieße. Ich bin allein und freue mich meines Lebens in dieser Gegend, die für solche Seelen geschaffen ist wie die meine. Ich bin so glücklich, mein Bester, so ganz in dem Gefühle von ruhigem Dasein versunken, daß meine Kunst darunter leidet. Ich könnte jetzt nicht zeichnen, nicht einen Strich, und bin nie ein größerer Maler gewesen als in diesen Augenblicken. Wenn das liebe Tal um mich dampft, und die hohe Sonne an der Oberfläche der undurchdringlichen Finsternis meines Waldes ruht.
    </p>
    <a href="<?php echo bloginfo('url'); ?>" class="button">Zur Startseite</a>
 </div>

</div><!-- /content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
