</main>

<footer class="footer">
  <div class="footer-inner">
    <div class="footer-address">
      <p>
        <b>StadtschülerInnenvertretung München</b><br>

        <br>Haus der Jugendarbeit 1. Stock<br>
        Rupprechtstraße 29<br>
        80636 München<br>
        <br>
        <a href="http://www.ssv-muenchen.de">www.ssv-muenchen.de</a><br>
        <a href="http://facebook.com/mucssv" target="blank">facebook.com/mucssv</a><br>
        <a href="mailto:ssv-muenchen.de">info@ssv-muenchen.de</a>
        <br><br>
        <a href="<?php echo bloginfo('url'); ?>/impressum">Impressum</a>
      </p>
      <img class="footer-kjr" src="<?php bloginfo('template_directory'); ?>/ressources/img/kjr_logo.png">
    </div>
  </div>

</footer>

</body>
</html>
