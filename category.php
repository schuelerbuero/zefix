<?php get_header(); ?>
<div class="content">
  <?php $i = 0; ?>
  <?php global $more; ?>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <?php if($i == 0){ ?>
        <?php $more = 1 ?>
        <div class="content-firstArticle">
            <a href="<?php the_permalink(); ?>"><h2 class="content-headline"><?php the_title();?></h2></a>
            <div class="content-content">
              <?php the_content(); ?>
            </div>
        </div><!-- /row -->
      <?php } else { ?>
        <?php $more = 0 ?>
        <?php if($i == 1){ ?>
          <?php $teasers = false; ?>
          <div class="grid teasers">
        <?php } ?>
            <div class="grid-item teaser">
              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail'); ?></a>
              <div class="teaser-inner">
                <a class="teaser-headline" href="<?php the_permalink(); ?>"><?php the_title();?></a>
                <div class="teaser-text">
                  <?php echo the_excerpt(); ?><a class="teaser-link" href="<?php the_permalink(); ?>">mehr lesen</a>
                </div><!-- /teaser-text -->
            </div><!-- /teaser-inner -->
          </div><!-- /teaser -->

      <?php if($i == $wp_query->found_posts && $teasers == true){ ?>
        </div><!-- /teasers> -->
      <?php } ?>
    <?php }?>
    <?php $i++; ?>
  <?php endwhile; else : ?>
  	<p><?php _e( 'Es wurden leider keine Beiträge zu dieser Kategorie gefunden.' ); ?></p>
  <?php endif; ?>
</div>
</div>
<!-- hullemulle -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
