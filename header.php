<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php bloginfo('name');
        wp_title('|'); ?></title>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/ressources/js/scripts.js"></script>

  </head>
  <body>
    <header class="header">
      <div class="header-inner">
          <a href="<?php echo bloginfo('url'); ?>"><img class="header-logo" src="<?php bloginfo('template_directory'); ?>/ressources/img/logo.png"></a>
        <nav class="header-navigation">
            <svg class="header-navigation-burger" height="32px" style="enable-background:new 0 0 32 32;" viewBox="0 0 32 32" width="32px">
              <path class="header-navigation-burger-path" d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/>
            </svg>
            <?php
              wp_nav_menu( array(
                'theme_location' => 'main-menu',
                'container_class'=>'header-navigation-menu',
                'container' => 'div',
                'items_wrap' => ' <ul>%3$s</ul>',
              ));
            ?>
        </nav>
      </div>
    </header>
    <img class="header-image" src="<?php bloginfo('template_directory'); ?>/ressources/img/header/<?php echo rand(1,6) ?>.jpg">

    <main class="main">
