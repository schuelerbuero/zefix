function setHeaderSticky(){
  var body = $('body');
  var header = $('header');
  var headerImage = $('.header-image');

  if (body.scrollTop() > 0) {
    header.addClass('header-sticky');
    headerImage.addClass('header-image--sticky-header');
  } else {
    header.removeClass('header-sticky');
    headerImage.removeClass('header-image--sticky-header');
  }
}

$(document).ready(function documentReady(){
  window.addEventListener('scroll', setHeaderSticky);
  var headerNavigationMenu = $('.header-navigation-menu');
  var headerNavigationBurger = $('.header-navigation-burger');

  headerNavigationBurger.click(function toggleMenuVisibility(){
    headerNavigationMenu.toggleClass('is-visible');
    headerNavigationBurger.toggleClass('header-navigation-burger--clicked');
  });
});

$(window).load(function windoLoad(){
  $('.grid').masonry({
    itemSelector: '.grid-item',
  });
});
